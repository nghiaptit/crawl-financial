import logging
import logging.config
import os

import yaml
from biz.crawl_biz import CrawlBiz

logger = logging.getLogger(__name__)


def prerequisites():
    output_path = 'output'
    if not os.path.exists(output_path):
        os.makedirs(output_path)


def conf_log():
    log_filename = "logs/crawl.log"
    os.makedirs(os.path.dirname(log_filename), exist_ok=True)

    with open("configs/logging.yaml", "r") as f:
        yaml_config = yaml.safe_load(f.read())
        logging.config.dictConfig(yaml_config)


def run():
    CrawlBiz().run()


if __name__ == '__main__':
    prerequisites()
    conf_log()
    run()
